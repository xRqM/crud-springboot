package com.crud.crud;

import com.crud.crud.model.Tutorial;
import com.crud.crud.repository.ITutorialRepository;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@Log4j2
class CrudApplicationTests {

	@Autowired
	private ITutorialRepository tutorialRepository;

	@Test
	void createTutorial() {
		Tutorial newTutorial = new Tutorial();
		newTutorial.setDescription("Tutorial utilizado para la implementacion de pruebas unitarias");
		newTutorial.setPublished(false);
		newTutorial.setTitle("Tutorial de Test");
		tutorialRepository.save(newTutorial);
	}

	@Test
	void findPublishedTutorials() {
		List<Tutorial> tutorialsFounds = tutorialRepository.findByPublished(true);
		assertThat(tutorialsFounds.size()).isEqualTo(1);
	}

	@Test
	void deleteTutorial() {
		tutorialRepository.deleteById(8L);
		List<Tutorial> allTutorials = tutorialRepository.findAll();

		assertThat(allTutorials.size()).isEqualTo(6);
	}

	@Test
	void updateTutorial() {
		List <Tutorial> tutorials = tutorialRepository.findAll();
		Tutorial tutorialUpdated = tutorials.get(1);
		tutorialUpdated.setTitle("Titulo cambiado");
		tutorialUpdated.setDescription("Descripcion cambiada");
		tutorialRepository.save(tutorialUpdated);
		assertThat(tutorialRepository.findAll().get(1).getTitle().compareTo("Titulo cambiado"));
	}

	@Test
	void getAllTutorials() {
		List <Tutorial> tutorials = tutorialRepository.findAll();
		assertThat(tutorials.size()).isEqualTo(6);
	}

	@Test
	void getTutorialById() {
		List <Tutorial> tutorials = tutorialRepository.findAll();
		assertThat(tutorials.get(2).getTitle().compareTo("Tutorial de Test"));
	}

}
