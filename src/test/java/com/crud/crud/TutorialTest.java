package com.crud.crud;

import com.crud.crud.controller.TutorialController;
import com.crud.crud.model.Tutorial;
import com.crud.crud.repository.ITutorialRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Log4j2
@RunWith(MockitoJUnitRunner.class)
public class TutorialTest {
    private MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();
    ObjectWriter objectWriter = objectMapper.writer();

    @Mock
    private ITutorialRepository tutorialRepository;

    @InjectMocks
    private TutorialController tutorialController;

    Tutorial tutorial1 = new Tutorial("Titulo 1", "Descripcion 1", true);
    Tutorial tutorial2 = new Tutorial("Titulo 2", "Descripcion 2", false);
    Tutorial tutorial3 = new Tutorial("Titulo 3", "Descripcion 3", true);

    @Before
    public void setUp(){
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(tutorialController).build();
        log.info(tutorial1.getId());
    }

    @Test
    public void getAllTutorials() throws Exception{
        List<Tutorial> allTutorials = new ArrayList<>(Arrays.asList(tutorial1,tutorial2,tutorial3));

        Mockito.when(tutorialRepository.findAll()).thenReturn(allTutorials);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/tutorials")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[2].title", is("Titulo 3")));
    }

    @Test
    public void getTutorialById() throws Exception{
        Mockito.when(tutorialRepository.findById(tutorial1.getId())).thenReturn(java.util.Optional.of(tutorial1));
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/tutorials/0")
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$", notNullValue()))
                        .andExpect(jsonPath("$.title", is("Titulo 1")));
    }

    @Test
    public void createTutorial() throws Exception {

        Tutorial newTutorial = Tutorial.builder()
                .title("Nuevo Titulo")
                .description("Nueva Descripcion")
                .published(true)
                .build();

        Mockito.lenient().when(tutorialRepository.save(newTutorial)).thenReturn(newTutorial);
        String content = objectWriter.writeValueAsString(newTutorial);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders
                .post("/api/tutorials")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(content);

        mockMvc.perform(mockRequest)
                .andExpect(status().isCreated());
    }

    @Test
    public void updateTutorial() throws Exception{
        Tutorial updatedTutorial = Tutorial.builder()
                .title("Titulo Cambiado")
                .description("Descripcion Cambiada")
                .published(true)
                .build();

        Mockito.when(tutorialRepository.findById(tutorial1.getId())).thenReturn(Optional.ofNullable(tutorial1));
        Mockito.when(tutorialRepository.save(updatedTutorial)).thenReturn(updatedTutorial);

        String updatedContent = objectWriter.writeValueAsString(updatedTutorial);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders
                .put("/api/tutorials/0")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(updatedContent);

        mockMvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.title", is("Titulo Cambiado")));
    }

    @Test
    public void deleteTutorial() throws Exception {

        Mockito.lenient().when(tutorialRepository.findById(tutorial1.getId())).thenReturn(Optional.of(tutorial1));

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/tutorials/0")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
