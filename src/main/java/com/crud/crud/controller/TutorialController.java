package com.crud.crud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.crud.crud.model.Tutorial;
import com.crud.crud.repository.ITutorialRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Clase Controller de la entidad Tutorial, contiene todas las funcionalidades CRUD de la API.
 * @author aolivarf
 */

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@Log4j2
@RequestMapping("/api")
public class TutorialController {
    @Autowired
    ITutorialRepository tutorialRepository;

    /**
     * Método que trae todos los tutoriales.
     * @return retorna una petición http dependiendo del resultado de la función.
     */
    @GetMapping("/tutorials")
    public ResponseEntity<List<Tutorial>> getAllTutorials(@RequestParam(required = false) String title) {
        try {
            List<Tutorial> tutorials = new ArrayList<Tutorial>();
            if (title == null) {
                tutorialRepository.findAll().forEach(tutorials::add);
                log.info("Desplegando todos los tutoriales sin un titulo relacionado");
            }
            else {
                tutorialRepository.findByTitleContaining(title).forEach(tutorials::add);
                log.info("Desplegando todos los tutoriales con el titulo relacionado");
            }
            if (tutorials.isEmpty()) {
                log.error("No hay tutoriales");
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(tutorials, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Método que trae un tutorial segun su id.
     * @param id id del objeto Tutorial que se quiere traer.
     * @return retorna una petición http y un objeto de tipo Tutorial, dependiendo del resultado de la función.
     */
    @GetMapping("/tutorials/{id}")
    public ResponseEntity<Tutorial> getTutorialById(@PathVariable("id") long id) {
        Optional<Tutorial> tutorialData = tutorialRepository.findById(id);
        if (tutorialData.isPresent()) {
            log.info("Se ha encontrado un tutorial con id: " + id);
            return new ResponseEntity<>(tutorialData.get(), HttpStatus.OK);
        } else {
            log.error("No se ha encontrado ningun tutorial");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Método que agrega un nuevo tutorial
     * @param tutorial objeto de tipo Tutorial
     * @return retorna una petición http, dependiendo del resultado de la función.
     */
    @PostMapping("/tutorials")
    public ResponseEntity<Tutorial> createTutorial(@RequestBody Tutorial tutorial) {
        try {
            Tutorial _tutorial = tutorialRepository.save(new Tutorial(tutorial.getTitle(), tutorial.getDescription(), false));
            log.info("Se ha creado un tutorial nuevo");
            return new ResponseEntity<>(_tutorial, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("Error" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Método que modifica un tutorial existente
     * @param id id del tutorial que se quiere editar
     * @return retorna una petición http y el tutorial editado para que sea guardado, dependiendo del resultado de la función.
     */
    @PutMapping("/tutorials/{id}")
    public ResponseEntity<Tutorial> updateTutorial(@PathVariable("id") long id, @RequestBody Tutorial tutorial) {
        Optional<Tutorial> tutorialData = tutorialRepository.findById(id);
        if (tutorialData.isPresent()) {
            Tutorial _tutorial = tutorialData.get();
            _tutorial.setTitle(tutorial.getTitle());
            _tutorial.setDescription(tutorial.getDescription());
            _tutorial.setPublished(tutorial.isPublished());
            log.info("Se ha editado el tutorial con id: " + id);
            return new ResponseEntity<>(tutorialRepository.save(_tutorial), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Método que elimina un tutorial
     * @param id id del tutorial que se quiere eliminar
     * @return retorna una petición http, dependiendo del resultado de la función.
     */
    @DeleteMapping("/tutorials/{id}")
    public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
        try {
            tutorialRepository.deleteById(id);
            log.info("Se ha eliminado el tutorial con id: " + id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Método elimina todos los tutoriales
     * @return retorna una petición http, dependiendo del resultado de la función.
     */
    @DeleteMapping("/tutorials")
    public ResponseEntity<HttpStatus> deleteAllTutorials() {
        try {
            tutorialRepository.deleteAll();
            log.info("Se han eliminado todos los tutoriales");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            log.error("Error" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Método que trae todos los tutoriales publicados
     * @return retorna una petición http y una lista de tutoriales publicados, dependiendo del resultado de la función.
     */
    @GetMapping("/tutorials/published")
    public ResponseEntity<List<Tutorial>> findByPublished() {
        try {
            List<Tutorial> tutorials = tutorialRepository.findByPublished(true);
            if (tutorials.isEmpty()) {
                log.info("No se han encontrado tutoriales publicados");
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            log.info("Se han encontrado tutoriales publicados");
            return new ResponseEntity<>(tutorials, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}