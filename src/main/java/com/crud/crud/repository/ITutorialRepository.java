package com.crud.crud.repository;

import com.crud.crud.model.Tutorial;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Clase Interface de la entidad Tutorial, contiene todas las funcionalidades del tipo JpaRepository ademas de dos adicionales relacionadas a la búsqueda.
 * @author aolivarf
 */

public interface ITutorialRepository extends JpaRepository<Tutorial, Long> {
    List<Tutorial> findByPublished(boolean published);
    List<Tutorial> findByTitleContaining(String title);
}
